import { AuthGuard } from './services/auth.guard';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

const routes: Route[] = [
  {
    path: '',
    redirectTo: 'budget',
    pathMatch: 'full'
  },
  {
    path: 'budget',
    loadChildren: './pages/budget/budget.module#BudgetPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    loadChildren: './pages/auth/auth.module#AuthPageModule'
  },
  {
    path: 'rate',
    loadChildren: './pages/rate/rate.module#RatePageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'result',
    loadChildren: './pages/result/result.module#ResultPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'statistics',
    loadChildren: './pages/statistics/statistics.module#StatisticsPageModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
