export interface MonthModel {
  id: number;
  name: string; // 'Август'
  num: string;  // '08'
}
