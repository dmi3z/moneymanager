export interface RateItem {
  date: number;
  name: string;
  price: number;
}

export interface RateTable {
  date: string;
  items: RateItem[];
}
