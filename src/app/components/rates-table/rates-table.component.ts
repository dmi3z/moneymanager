import { RateItem } from './../../models/rate.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { RateTable } from 'src/app/models/rate.model';

@Component({
  selector: 'app-rates-table',
  templateUrl: './rates-table.component.html',
  styleUrls: ['./rates-table.component.scss']
})

export class RatesTableComponent {

  @Input() tableItems: RateTable;
  @Output() deleteEvent = new EventEmitter<RateItem>();

  constructor() {}

  public onDeleteRate(item: RateItem): void {
    this.deleteEvent.emit(item);
  }

  public get totalSumm(): number {
    let summ = 0;
    this.tableItems.items.forEach(item => {
      summ += +item.price;
    });
    return +summ;
  }

}
