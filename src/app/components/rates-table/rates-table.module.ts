import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RatesTableComponent } from './rates-table.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { TableRowComponent } from './table-row/table-row.component';

@NgModule({
  declarations: [
    RatesTableComponent,
    TableRowComponent
  ],
  imports: [
    CommonModule,
    PipesModule,
    FormsModule
  ],
  exports: [
    RatesTableComponent
  ]
})

export class RatesTableModule {}
