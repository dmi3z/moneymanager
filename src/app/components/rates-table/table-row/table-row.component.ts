import { RateItem } from './../../../models/rate.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-row',
  templateUrl: './table-row.component.html',
  styleUrls: ['./table-row.component.scss']
})

export class TableRowComponent {

  @Input() item: RateItem;
  public isEditState: boolean;
  @Output() deleteEvent = new EventEmitter<RateItem>();

  constructor() {}

  public toggleState(): void {
    this.isEditState = !this.isEditState;
  }

  public deleteItem(): void {
    this.deleteEvent.emit(this.item);
  }
}

