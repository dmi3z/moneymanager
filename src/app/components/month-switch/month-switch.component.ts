import { MonthModel } from './../../models/date.mode';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-month-switch',
  templateUrl: './month-switch.component.html',
  styleUrls: ['./month-switch.component.scss']
})

export class MonthSwitchComponent implements OnInit {

  public monthData: MonthModel[] = [
    {
      id: 0,
      num: '01',
      name: 'Январь'
    },
    {
      id: 1,
      num: '02',
      name: 'Февраль'
    },
    {
      id: 2,
      num: '03',
      name: 'Март'
    },
    {
      id: 3,
      num: '04',
      name: 'Апрель'
    },
    {
      id: 4,
      num: '05',
      name: 'Май'
    },
    {
      id: 5,
      num: '06',
      name: 'Июнь'
    },
    {
      id: 6,
      num: '07',
      name: 'Июль'
    },
    {
      id: 7,
      num: '08',
      name: 'Август'
    },
    {
      id: 8,
      num: '09',
      name: 'Сентябрь'
    },
    {
      id: 9,
      num: '10',
      name: 'Октябрь'
    },
    {
      id: 10,
      num: '11',
      name: 'Ноябрь'
    },
    {
      id: 11,
      num: '12',
      name: 'Декабрь'
    }
  ];

  public currentMonth: MonthModel;
  public currentMonthId = 0;
  @Output() monthChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {
    moment.locale('ru');
    const currentMonthName = moment().format('MMMM');
    this.currentMonth = this.monthData.find(month => month.name.toLowerCase() === currentMonthName);
    this.currentMonthId = this.currentMonth.id;
  }

  public onMonthChange(): void {
    const num = this.monthData.find(item => +item.id === +this.currentMonthId).num;
    this.monthChange.emit(num);
  }
}
