import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MonthSwitchComponent } from './month-switch.component';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [
    MonthSwitchComponent,
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    MonthSwitchComponent
  ]
})

export class MonthSwitchModule {}
