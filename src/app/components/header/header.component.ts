import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {

  constructor(private authService: AuthService, private router: Router) {}

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public get userName(): string {
    return this.authService.userName;
  }

  public login(): void {
    if (this.isLogin) {
      this.authService.logout();
    } else {
      this.router.navigate(['auth']);
    }
  }

}
