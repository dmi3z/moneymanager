import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StatisticsPageComponent } from './statistics.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    StatisticsPageComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    RouterModule.forChild([
      {
        path: '',
        component: StatisticsPageComponent
      }
    ])
  ]
})

export class StatisticsPageModule {}
