import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ResultPageComponent } from './result.component';

@NgModule({
  declarations: [
    ResultPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ResultPageComponent
      }
    ])
  ]
})

export class ResultPageModule {}
