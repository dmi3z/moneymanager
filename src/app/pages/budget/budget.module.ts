import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { BudgetPageComponent } from './budget.component';

@NgModule({
  declarations: [
    BudgetPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: BudgetPageComponent
      }
    ])
  ]
})

export class BudgetPageModule {}
