import { User } from './../../models/user.model';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthPageComponent implements OnInit {

  public login: FormControl;
  public password: FormControl;
  public authForm: FormGroup;

  public isValuesCorrect = true;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.createFormFields();
    this.createFormGroup();
  }

  public authorization(): void {
    if (this.authForm.valid) {
      const user: User = this.authForm.value;
      this.isValuesCorrect = this.authService.login(user);
    }
  }

  private createFormFields(): void {
    this.login = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(6)]);
  }

  private createFormGroup(): void {
    this.authForm = new FormGroup({
      login: this.login,
      password: this.password
    });
  }

}
