import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { RateItem } from '../../../models/rate.model';

@Component({
  selector: 'app-rate-item',
  templateUrl: './rate-item.component.html',
  styleUrls: ['./rate-item.component.scss']
})

export class RateItemComponent {

  @Input() item: RateItem;

  @ViewChild('rateRadio') radio: ElementRef;
  @ViewChild('inpPrice') inpPrice: ElementRef;

  @Output() payConfirm = new EventEmitter<RateItem>();
  public price = 0;

  constructor() {}

  public onPriceSave(): void {
    if (+this.price !== 0) {
      const radio: HTMLInputElement = this.radio.nativeElement;
      radio.checked = false;
      this.item.price = this.price;
      this.payConfirm.next(this.item);
    }
  }

  public onPriceFocus(): void {
    if (+this.price === 0) {
      this.price = null;
    }
  }

  public onPriceBlur(): void {
    if (this.price === null) {
      this.price = 0;
    }
  }

  public onItemClick(): void {
    this.inpPrice.nativeElement.focus();
  }

}
