import { MonthSwitchModule } from './../../components/month-switch/month-switch.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RatePageComponent } from './rate.component';
import { RateItemComponent } from './rate-item/rate-item.component';
import { RatesTableModule } from 'src/app/components/rates-table/rates-table.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  declarations: [
    RatePageComponent,
    RateItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RatesTableModule,
    PipesModule,
    MonthSwitchModule,
    RouterModule.forChild([
      {
        path: '',
        component: RatePageComponent
      }
    ])
  ]
})

export class RatePageModule {}
