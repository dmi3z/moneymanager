import { Component, OnInit } from '@angular/core';
import { RateItem, RateTable } from '../../models/rate.model';

import * as moment from 'moment';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-rate-page',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})

export class RatePageComponent implements OnInit {

  private rateNames: string[] = [];

  public rateItems: RateItem[] = [];
  public rates: RateTable;
  public isAddState: boolean;

  public rateName = '';
  private unixDate: string;

  constructor(private dataService: DataService) {}

  ngOnInit() {

    this.rateNames = this.dataService.getRateNames();

    this.unixDate = moment().format('MM-YYYY');

    const allRates = this.dataService.getRates();

    this.rateNames.forEach(item => {
      this.rateItems.push({
        date: moment().unix(),
        name: item,
        price: 0
      });
    });

    if (allRates && allRates.length > 0) {
      const currentMonth = allRates.find(month => month.date === this.unixDate);
      if (currentMonth) {
        this.rates = currentMonth;
        this.rates.items.forEach(element => {
          const item = this.rateItems.find(r => r.name === element.name);
          const index = this.rateItems.indexOf(item);
          this.rateItems.splice(index, 1);
        });
      } else {
        this.rates = {
          date: this.unixDate,
          items: []
        };
      }
    }
  }

  public onPayConfirm(item: RateItem): void {
    item.date = moment().unix();
    this.rates.items.push(item);
    const index = this.rateItems.indexOf(item);
    this.rateItems.splice(index, 1);
    this.dataService.saveRate(item);
  }

  public get isHaveRates(): boolean {
    return this.rates.items.length > 0;
  }

  public toggleAddState(): void {
    this.isAddState = !this.isAddState;
  }

  public saveRate(): void {
    if (this.rateName.length > 0) {
      this.rateItems.push({
        date: 0,
        name: this.rateName,
        price: 0
      });
      this.dataService.addRateName(this.rateName);
      this.rateName = '';
      this.toggleAddState();
    }
  }

  public onDeleteRate(item: RateItem): void {
    const index = this.rates.items.indexOf(item);
    this.rateItems.push({
      date: 0,
      name: item.name,
      price: 0
    });
    this.rates.items.splice(index, 1);
    this.dataService.deleteRate(this.unixDate, item);
  }

  public onMonthChange(num: string): void {
    console.log(num);
  }

}
