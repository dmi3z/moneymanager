import { CommonModule } from '@angular/common';
import { MonthPipe } from './month.pipe';
import { DateTimePipe } from './datetime.pipe';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    DateTimePipe,
    MonthPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DateTimePipe,
    MonthPipe
  ]
})

export class PipesModule {}
