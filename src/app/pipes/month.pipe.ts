import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'monthPipe'
})

export class MonthPipe implements PipeTransform {
  transform(value: string) {
    moment.locale('ru');
    return moment(value, 'MM-YYYY').format('MMMM');
  }
}
