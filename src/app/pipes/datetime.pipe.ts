import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateTimePipe'
})

export class DateTimePipe implements PipeTransform {
  transform(value: string) {
    return moment.unix(+value).format('DD-MM-YYYY HH:mm');
  }
}
