import { RateItem, RateTable } from './../models/rate.model';
import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class DataService {

  private readonly rateNames = ['Электроэнергия', 'Комуналка', 'Газ', 'Телефон', 'Интернет'];

  constructor() { }

  getRateNames(): string[] {
    const rateNames = JSON.parse(localStorage.getItem('ratenames'));
    if (rateNames) {
      return rateNames;
    } else {
      localStorage.setItem('ratenames', JSON.stringify(this.rateNames));
      return this.rateNames;
    }
  }

  addRateName(rate: string): void {
    let rateNames = JSON.parse(localStorage.getItem('ratenames'));
    if (!rateNames) {
      rateNames = this.rateNames;
    }
    rateNames.push(rate);
    localStorage.setItem('ratenames', JSON.stringify(rateNames));
  }

  saveRate(rate: RateItem): void {
    let rates: RateTable[] = JSON.parse(localStorage.getItem('rates'));
    if (rates) {
      let currentMonth = rates.find(item => item.date === moment.unix(rate.date).format('MM-YYYY'));
      if (currentMonth) {
        const order = currentMonth.items.find(or => or.name === rate.name);
        if (order) {
          order.price = rate.price;
        } else {
          currentMonth.items.push(rate);
        }
      } else {
        currentMonth = {
          date: moment().format('MM-YYYY'),
          items: [rate]
        };
        rates.push(currentMonth);
      }
    } else {
      const month: RateTable = {
        date: moment.unix(rate.date).format('MM-YYYY'),
        items: [rate]
      };
      rates = [month];
    }
    localStorage.setItem('rates', JSON.stringify(rates));
  }

  getRates(): RateTable[] {
    const rates: RateTable[] = JSON.parse(localStorage.getItem('rates'));
    if (rates) {
      return rates;
    }
    return [];
  }

  deleteRate(date: string, item: RateItem): void {
    const rates = this.getRates();
    const month = rates.find(rate => rate.date === date);
    const order = month.items.find(rate => rate.name === item.name);
    const index = month.items.indexOf(order);
    month.items.splice(index, 1);
    localStorage.setItem('rates', JSON.stringify(rates));
  }
}
