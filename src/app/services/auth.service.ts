import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable()
export class AuthService {

  constructor(private router: Router) {}

  public login(user: User): boolean {
    if (user.password === '123456') {
      this.setAuth(user.login);
      this.router.navigate(['budget']);
    } else {
      return false;
    }
  }

  public get isLogin(): boolean {
    const isauth = localStorage.getItem('isauth');
    if (isauth) {
      return true;
    }
    return false;
  }

  public logout(): void {
    localStorage.removeItem('isauth');
    this.router.navigate(['auth']);
  }

  public get userName(): string {
    return localStorage.getItem('isauth');
  }

  private setAuth(login: string): void {
    localStorage.setItem('isauth', login);
  }

}

